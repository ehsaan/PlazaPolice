const Telegraf = require('telegraf'),
      session = require('telegraf/session')
      bot = new Telegraf(process.env.BOT_TOKEN),
      groupID = -1001206727777,
      channelID = -1001222498748,

bot.use(session())
global.bot = bot

const { deleteMessage, isAdmin, mention } = require('./utils')
const { queryAll, query, add, remove } = require('./frequentHashtags')

bot.telegram.getMe().then((profile) => {
    global.botID = profile.id
    bot.options.username = profile.username

    console.log(`bot is running at https://t.me/${profile.username}`)
})

/**
 * Administrator's private chat commands
 * 1. /addFrequent
 * 2. /delFrequent
 * 3. /listFrequents
 */
bot.hears(/\/addFrequent/, async(ctx, next) => {
    if ( ctx.chat.type != 'private' )
        return ctx.deleteMessage()
    
    if ( ! await isAdmin(groupID, ctx.from.id) )
        return next()

    ctx.session.stage = 'wait hashtag'
    ctx.reply('لطفاً هشتگ موردنظر را بدون # ارسال کنید (برای انصراف می‌توانید از دستور /cancel استفاده کنید)')
})

bot.on('text', async(ctx, next) => {
    if ( ! await isAdmin(groupID, ctx.from.id) )
        return next()

    if ( ctx.session && ctx.session.stage && ctx.message.text == '/cancel' ) {
        ctx.session.stage = null

        return ctx.reply('فرآیند متوقف شد.', {
            reply_to_message_id:        ctx.message.message_id,
        })
    }

    if ( ctx.session.stage == 'wait hashtag' ) {
        ctx.session.tag = ctx.message.text
        ctx.session.stage = 'wait text'

        return ctx.reply('بسیار عالی! حالا توضیحاتی که با ارسال این هشتگ باید ارسال شود را بنویسید. (فرمت HTML قابل‌قبول است)', {
            reply_to_message_id:            ctx.message.message_id
        })
    }
    if ( ctx.session.stage == 'wait text' ) {
        ctx.session.text = ctx.message.text
        ctx.session.stage = 'wait confirm'

        return ctx.replyWithHTML(`آیا از ثبت این جزئیات در سیستم مطمئن هستید؟

#${ctx.session.tag}
${ctx.session.text}`,
        {
            reply_markup:           {
                keyboard:           [ [ 'بله', 'خیر' ] ],
                one_time_keyboard:  true,
                resize_keyboard:    true,
            }
        })
    }

    if ( ctx.session.stage == 'wait confirm' ) {
        if ( ctx.message.text == 'خیر' ) {
            ctx.session = {}

            return ctx.reply('بسیار خب (:', {
                reply_to_message_id:        ctx.message.message_id,
            })
        }

        add(ctx.session.tag, ctx.session.text)
        ctx.session = {}

        return ctx.reply('جزئیات هشتگ جدید در سیستم ثبت شد (:')
    }

    next()
})

bot.hears(/\/delFrequent (.?)/, async(ctx, next) => {
    if ( ctx.chat.type != 'private' )
        return deleteMessage(ctx.message)
    
    if ( ! await isAdmin(groupID, ctx.from.id) )
        return next()

    if ( ! ctx.match[1] )
        return ctx.reply('لطفاً هشتگ موردنظر را بدون # روبه‌روی دستور وارد کنید. برای مثال:\n/delFrequent تست')
    
    remove(ctx.match[1])
    ctx.reply('انجام شد!')
})

bot.hears(/\/listFrequents/, async(ctx, next) => {
    if ( ctx.chat.type != 'private' )
        return deleteMessage(ctx.message)
    
    if ( ! await isAdmin(groupID, ctx.from.id) )
        return next()
    
    const frequents = await queryAll()

    var output = ''
    for ( let tag of Object.keys(frequents) )
        output += `— #${tag}\n${frequents[tag]}\n\n`
    
    ctx.replyWithHTML(output, {
        disable_web_page_preview:       true,
    })
})

bot.use(async (ctx, next) => {
    if ( ctx.chat.type != 'private' && ctx.chat.id != groupID && ctx.chat.id != channelID )
        ctx.leaveChat()
    
    else if ( ctx.chat.type == 'private' ) {
        if ( ! await isAdmin(groupID, ctx.from.id) )
            ctx.replyWithHTML('ربات <b>Plaza Police</b>\n\nhttps://gitlab.com/ehsaan/PlazaPolice', {
                disable_web_page_preview:               true,
            })
        else {
            ctx.reply(`
            در چت خصوصی بات (مخصوص ادمین‌ها):
— دستور addFrequent/
با این دستور می‌تونید هشتگ‌های پرکاربرد رو اضافه کنید. مثلا هشتگ #انویدیا رو وارد می‌کنید و بعدش یه متن دلخواه. وقتی ثبت بشه، از اون به بعد ربات هر پیامی رو که با اون هشتگ تو گروه بگیره، همون متنی که شما بهش دادید رو به کاربر جواب میده.

— دستور delFrequent/
بعد از خود دستور با یه فاصله باید اون هشتگی رو که می‌خواید حذف کنید وارد کنید. مثلا:
/delFrequent تست

— دستور listFrequents/
لیست هشتگ‌های موجود در سیستم.

در گروه (مخصوص ادمین‌ها):
— دستور gag/
می‌تونید در پاسخ پیام یه کاربر، این دستور رو بفرستید. دقایق گگ‌بودن (محرومیت از ارسال پیام) کاربر رو مشخص کنید. مثلا
/gag 5
که برای ۵ دقیقه از نوشتن محرومش می‌کنه

— «لینک گروه»
خود این عبارت، لینک گروه رو میده و بعد ۳۰ ثانیه پاک می‌کنه.

— هشتگ موقت
پیام رو بعد از ۳۰ ثانیه اتوماتیک حذف می‌کنه.
            `)
        }
    }
    
    next()
})

/**
 * Administrator's in-group gag command
 */

bot.hears(/\/gag (\d+)/, async (ctx) => {
    if ( ! await isAdmin(ctx.chat.id, ctx.from.id) )
        return deleteMessage(ctx.message)

    var minutes = +ctx.match[1].trim()
    if ( ! minutes || minutes < 0 || minutes > 60 )
        minutes = 5

    
    const target = ctx.message.reply_to_message

    if ( ! target || target.from.id == ctx.from.id
                  || isAdmin(ctx.chat.id, target.from.id) )
        return deleteMessage(ctx.message)
    
    const until_date = Math.floor(new Date() / 1000) + (minutes * 60)
    bot.telegram.restrictChatMember(ctx.chat.id, target.from.id, {
        can_send_messages:              false,
        until_date,
    })

    ctx.deleteMessage()
    ctx.replyWithHTML(`کاربر ${mention(target.from)} به مدت <b>${minutes}</b> دقیقه از ارسال هر گونه پیام توسط ادمین ${mention(ctx.from)} منع گردید.`, {
        disable_web_page_preview:           true
    })
})

bot.hears('لینک گروه', async (ctx) => {
    try {
        const chatLink = await ctx.exportChatInviteLink(),
              thisChat = await ctx.getChat()

        ctx.replyWithHTML(`گروه <b>${thisChat.title}</b>

<a href="${chatLink}">عضویت</a>`, {
            reply_to_message_id:        ctx.message.message_id,
            disable_web_page_preview:   true,
        }).then((sent) => {
           setTimeout(() => {
                deleteMessage(sent)
                ctx.deleteMessage()
           }, 30000)
        })
    } catch(e) {
        console.error(e)
    }
})

bot.hears(/#موقت/, async (ctx) => {
    if ( ! await isAdmin(ctx.chat.id, ctx.from.id) )
        return false

    setTimeout(() => {
        deleteMessage(ctx.message)
    }, 30000)
})

/**
 * Reporting system
 */
bot.hears(/^[!\/]report$/, async (ctx) => {
    if ( ! ctx.message.reply_to_message )
        return ctx.deleteMessage()
    
    if ( ! isAdmin(ctx.chat.id, ctx.message.reply_to_message.from.id) )
        return ctx.deleteMessage()
    
    const reportedMessage = ctx.message.reply_to_message
    
    bot.telegram.forwardMessage(channelID, ctx.chat.id, reportedMessage.message_id).then((msg) => {
        bot.telegram.sendMessage(channelID, `گزارش توسط ${mention(ctx.from)}`, {
            reply_markup:                   {
                inline_keyboard:            [
                    [ { text: 'بررسی شد', callback_data: 'review' } ],
                    [ { text: 'اخراج کاربر از گروه', callback_data: 'kick' } ],
                ]
            },
            reply_to_message_id:            msg.message_id,
            parse_mode:                     'HTML',
        })
    })

    ctx.deleteMessage()
})
/**
 * Report channel actions
 */
bot.action('review', (ctx) => {
    ctx.editMessageText('' + ctx.callbackQuery.message.text + `\n\n✅ این گزارش توسط ${mention(ctx.callbackQuery.from)} بررسی شد.`, {
        parse_mode:                 'HTML',
    })
})

bot.action('kick', (ctx) => {
    const targetID = ctx.callbackQuery.message.reply_to_message.forward_from.id
    bot.telegram.kickChatMember(groupID, targetID).catch(() => {})

    ctx.editMessageText('' + ctx.callbackQuery.message.text + `\n\n✅ این گزارش توسط ${mention(ctx.callbackQuery.from)} بررسی شد و کاربر اخراج گردید.`, {
        parse_mode:                 'HTML',
    })
})

/**
 * Misc commands
 */

bot.on('message', async(ctx, next) => {
    const ents = ( ( ctx.message.text ) ? ctx.message.entities : ctx.message.caption_entities ) || []
    
    for ( let ent of ents ) {
        if ( ent.type && ent.type == 'mention' ) {
            if ( isAdmin(ctx.chat.id, ctx.from.id) )
                continue
            
            const mentioned = ctx.message.text.substr(ent.offset, ent.length)
            try {
                const chat = await ctx.telegram.getChat(mentioned)
                if ( chat && chat.type == 'channel' ) 
                    return deleteMessage(ctx.message)
                
            } catch( e ) {
                console.error(e)
            }
        }

        if ( ent.type && ent.type == 'hashtag' ) {
            const hashtag = ctx.message.text.substr(ent.offset + 1, ent.length - 1),
                  message = await query(hashtag)

            if ( message )
                return ctx.replyWithHTML(message, {
                    disable_web_page_preview:   true,
                    reply_to_message_id:        ctx.message.reply_to_message ? ctx.message.reply_to_message.message_id : null
                })
        }
    }

    next()
})

bot.use(async (ctx, next) => {
    try {
        if ( await isAdmin(ctx.chat.id, ctx.from.id) )
            return false // From this point, we won't process admin messages.
        
        if ( ctx.from.id == botID )
            return false // Neither the bot messages
    } catch(e) {
        console.error(e)
    }

    next()
})

bot.on('message', (ctx, next) => {
    if ( ctx.message.left_chat_member )
        return deleteMessage(ctx.message)
    
    if ( ctx.message.forward_from_chat && ctx.message.forward_from_message_id )
        return deleteMessage(ctx.message)
    
    var newMembers = ctx.message.new_chat_members
    if ( ! newMembers )
        return next()

    deleteMessage(ctx.message)

    for ( let member of newMembers ) {
        if ( member.id == botID )
            continue
        
        if ( member.is_bot )
            ctx.telegram.kickChatMember(ctx.chat.id, member.id)
    }
})

bot.on(['sticker', 'video_note', 'voice', 'animation'], (ctx) => {
    ctx.telegram.restrictChatMember(ctx.chat.id, ctx.from.id, {
        can_send_messages:                      true,
        can_send_media_messages:                true,
        can_send_other_messages:                false,
        can_add_web_page_previews:              false
    })
    deleteMessage(ctx.message)
})

bot.hears([/t(elegram)?\.me/, /tlgrm\.me/], ctx => ctx.deleteMessage())

bot.startPolling()
const fs = require('fs')

module.exports = {
    add: (tag, text) => {
        var tags = require('./common.json')
        tags[tag] = text
        
        return fs.writeFileSync('./common.json', JSON.stringify(tags))
    },
    remove: (tag, text) => {
        var tags = require('./common.json')
        delete tags[tag]

        return fs.writeFileSync('./common.json', JSON.stringify(tags))
    },
    queryAll: () => {
        var tags = require('./common.json')
        return tags
    },
    query: (tag) => {
        var tags = require('./common.json')
        return tags[tag]
    }
}
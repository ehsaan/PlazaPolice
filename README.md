# Plaza Police
Plaza Police is a Telegram supergroup management assistant. It does not ban or kick any users unless they are bots.

## Deployment
Plaza Police image is available at [Fandogh PaaS](https://fandogh.cloud) under the name "plazapolicebot".
